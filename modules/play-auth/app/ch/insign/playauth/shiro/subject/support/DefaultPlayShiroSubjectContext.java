/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.playauth.shiro.subject.support;

import org.apache.shiro.subject.Subject;
import org.apache.shiro.subject.support.DefaultSubjectContext;

import play.mvc.Http.Context;

import ch.insign.playauth.shiro.subject.PlayShiroSubject;

public class DefaultPlayShiroSubjectContext extends DefaultSubjectContext implements
        ch.insign.playauth.shiro.subject.PlayShiroSubjectContext {

    private static final long serialVersionUID = 1896848921742292018L;

    private static final String HTTP_CONTEXT = DefaultPlayShiroSubjectContext.class.getName() + ".HTTP_CONTEXT";

    public DefaultPlayShiroSubjectContext() {
    }

    public DefaultPlayShiroSubjectContext(ch.insign.playauth.shiro.subject.PlayShiroSubjectContext context) {
        super(context);
    }

    @Override
    public String resolveHost() {
        String host = super.resolveHost();
        if (host == null) {
            Context ctx = resolveHttpContext();
            if (ctx != null) {
                host = ctx.request().host();
            }
        }
        return host;
    }

    @Override
    public Context getHttpContext() {
        return getTypedValue(HTTP_CONTEXT, Context.class);
    }

    @Override
    public void setHttpContext(Context context) {
        if (context != null) {
            put(HTTP_CONTEXT, context);
        }
    }

    @Override
    public Context resolveHttpContext() {
        Context context = getHttpContext();

        // fall back on existing subject instance if it exists:
        if (context == null) {
            Subject existing = getSubject();
            if (existing instanceof PlayShiroSubject) {
                context = ((PlayShiroSubject) existing).getHttpContext();
            }
        }

        return context;
    }

}
