/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.commons.db;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.data.validation.ValidationError;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Timo Schmid <timo.schmid@gmail.com>
 */

public class DataBinding  {
	private final static Logger logger = LoggerFactory.getLogger(DataBinding.class);

    private DataBinding() {}

    private static Map<Class<?>, DataBinder> registeredDataBinders = new HashMap<>();

    public static boolean isSupported(Class<?> clazz) {
        return registeredDataBinders.containsKey(clazz);
    }

    public static void registerDataBinder(Class<?> clazz, DataBinder dataBinder) {
        if(registeredDataBinders.containsKey(clazz)) {
            logger.info("There already is a DataBinder registered for the class " + clazz.getSimpleName() +". Skipping.");
            return;
        }
        registeredDataBinders.put(clazz, dataBinder);
    }

    public static Map<String,String> getMap(Class<?> clazz, String fieldName, Object o) {
        return getDataBinder(clazz).toData(fieldName, o);
    }

    public static Map<String,List<ValidationError>> validate(String fieldName, Object o, Object entityObject,
                                                             Map<String, String> formData) {
        return getDataBinder(o.getClass()).validate(fieldName, o, entityObject, formData);
    }

    private static DataBinder getDataBinder(Class<?> clazz) {
        if(isSupported(clazz)) {
            return registeredDataBinders.get(clazz);
        } else {
            throw new IllegalArgumentException("There is no DataBinder registered for the class " + clazz.getSimpleName() + ". Did you forget to register the DataBinder?");
        }
    }

    public static Object bind(Class<?> clazz, String fieldName, Object object, Map<String, String> values) {
        return getDataBinder(clazz).getFromData(fieldName, object, values);
    }


    public interface DataBinder {

        Object getFromData(String fieldName, Object instance, Map<String, String> data);

        Map<String,String> toData(String fieldName, Object t);

        Map<String,List<ValidationError>> validate(String fieldName, Object object, Object formEntityObject,
                                                   Map<String, String> formData);

    }

}
