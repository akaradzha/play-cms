/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.attributeset.model;

import play.data.format.Formatters;

import java.text.ParseException;
import java.util.Locale;

public class AttributeFormatter  extends Formatters.SimpleFormatter<Attribute> {

    @Override
    public Attribute parse(String input, Locale locale) {
        return Attribute.find.byId(input);
    }

    @Override
    public String print(Attribute attribute, Locale locale) {
        return attribute.getId();
    }
}
