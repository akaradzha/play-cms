jQuery(document).ready(function($) {
    // updates review status
    $("#review_status_button").on("click", ".link-change-review", function(e) {
        e.preventDefault();
        $.ajax({
            type: 'POST',
            url: $(this).data('url'),
            success: function(result) {
                $('#review_status_button').html(result);
            }
        });
    });
});
