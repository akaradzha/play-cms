/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.commons.filter;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

/**
 * Marks a class as applying content filters through FilterManager.
 *
 * Note: the implementing class is solely responsible of the way it treats filters,
 * e.g. only on input or also on output (output is usually not done in beans but
 * by controllers, to distinguish between admin editing and frontend display).
 *
 * @author bachi
 */
public interface Filterable {

    public void filterInputData();
    public void filterOutputData();
}
