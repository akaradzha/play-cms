package ch.insign.playauth.authz;

import ch.insign.playauth.authz.AccessControlListVoter.Vote;
import ch.insign.playauth.authz.mock.AccessControlListMock;
import ch.insign.playauth.authz.mock.PermissionManagerProviderMock;
import ch.insign.playauth.authz.support.DefaultAccessControlListVoter;
import ch.insign.playauth.authz.support.DefaultObjectIdentityRetrievalStrategy;
import ch.insign.playauth.authz.support.DefaultPermissionManager;
import ch.insign.playauth.authz.support.DefaultSecurityIdentityRetrievalStrategy;
import ch.insign.playauth.event.EventDispatcher;
import org.junit.Test;
import play.Environment;
import play.inject.Injector;

import java.util.Optional;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class AccessControlListVoterTest {

    @WithIdentifier(PermissionManagerTest.Domain1Idetifier.class)
    public interface Domain1 {
        String getId();
    }

    public static class Domain1Idetifier implements Identifier {
        @Override
        public Optional<String> getIdentifier(Object obj) {
            if (obj instanceof PermissionManagerTest.Domain1) {
                return Optional.of((PermissionManagerTest.Domain1) obj).map(PermissionManagerTest.Domain1::getId);
            }
            return Optional.empty();
        }
    }

    @Test
    public void vote() {
        Injector injector = mock(Injector.class);
        when(injector.instanceOf(Domain1Idetifier.class)).thenReturn(new Domain1Idetifier());

        EventDispatcher dispatcher = new EventDispatcher();
        AccessControlList acl = new AccessControlListMock();
        PermissionManagerProviderMock pmp = new PermissionManagerProviderMock();
        ObjectIdentityRetrievalStrategy oidrs = new DefaultObjectIdentityRetrievalStrategy(pmp, injector);
        SecurityIdentityRetrievalStrategy sidrs = new DefaultSecurityIdentityRetrievalStrategy();
        PermissionManager pm = pmp.set(new DefaultPermissionManager(acl, oidrs, null, Environment.simple())).get();

        AccessControlListVoter aclv = new DefaultAccessControlListVoter(dispatcher, pm, acl, sidrs, oidrs);

        // Authorities
        SecurityIdentity anyone = SecurityIdentity.ALL;
        SecurityIdentity party1 = new SecurityIdentity("Party", "1");
        SecurityIdentity party2 = new SecurityIdentity("Party", "2");

        // Resources
        ObjectIdentity anything = oidrs.getObjectIdentity(Domain1.class);
        ObjectIdentity resource1 = oidrs.getObjectIdentity((Domain1) () -> "1");
        ObjectIdentity resource2 = oidrs.getObjectIdentity((Domain1) () -> "2");

        // Permissions
        DomainPermission<?> permRead = pm.definePermission(Domain1.class, "read");
        DomainPermission<?> permReadAnything = pm.applyTarget(permRead, anything);
        DomainPermission<?> permReadResource1 = pm.applyTarget(permRead, resource1);
        DomainPermission<?> permReadResource2 = pm.applyTarget(permRead, resource2);

        DomainPermission<?> permWrite = pm.definePermission(Domain1.class, "write");
        DomainPermission<?> permWriteAnything = pm.applyTarget(permWrite, anything);
        DomainPermission<?> permWriteResource1 = pm.applyTarget(permWrite, resource1);
        DomainPermission<?> permWriteResource2 = pm.applyTarget(permWrite, resource2);

        // anyone:denied:write:anything
        acl.put(new AccessControlEntry(anyone, anything)).deny(permWriteAnything);

        // party2:allowed:read,write:resource2
        acl.put(new AccessControlEntry(party2, resource2).allow(permReadResource2).allow(permWriteResource2));


        assertTrue("`anyone:?:write:anything`",
                aclv.vote(anyone, permWriteAnything).isPresent());

        assertFalse("`anyone:allowed:write:anything`",
                aclv.vote(anyone, permWriteAnything).filter(Vote::allowed).isPresent());

        assertFalse("`anyone:?:read:anything`",
                aclv.vote(anyone, permReadAnything).isPresent());


        assertFalse("`party1:?:read:resource1`",
                aclv.vote(party1, pm.applyTarget(permReadResource1, resource1)).isPresent());

        assertFalse("`party1:?:read:resource2`",
                aclv.vote(party1, pm.applyTarget(permReadResource2, resource2)).isPresent());

        assertTrue("`party1:?:write:resource1`",
                aclv.vote(party1, pm.applyTarget(permWriteResource1, resource1)).isPresent());
        assertFalse("`party1:allowed:write:resource1`",
                aclv.vote(party1, pm.applyTarget(permWriteResource1, resource1)).filter(Vote::allowed).isPresent());

        assertTrue("`party1:?:write:resource2`",
                aclv.vote(party1, pm.applyTarget(permWriteResource2, resource2)).isPresent());
        assertFalse("`party1:allowed:write:resource2`",
                aclv.vote(party1, pm.applyTarget(permWriteResource2, resource2)).filter(Vote::allowed).isPresent());

        assertTrue("`party1` inherits `anyone:?:write:anything`",
                aclv.vote(party1, permWriteAnything).filter(v -> v.sid().equals(anyone)).isPresent());


        assertFalse("`party2:?:read:resource1`",
                aclv.vote(party2, pm.applyTarget(permReadResource1, resource1)).isPresent());

        assertTrue("`party2:?:read:resource2`",
                aclv.vote(party2, pm.applyTarget(permReadResource2, resource2)).isPresent());
        assertFalse("`party2:denied:read:resource2`",
                aclv.vote(party2, pm.applyTarget(permReadResource2, resource2)).filter(Vote::denied).isPresent());

        assertTrue("`party2:?:write:resource1`",
                aclv.vote(party2, pm.applyTarget(permWriteResource1, resource1)).isPresent());
        assertFalse("`party2:allowed:write:resource1`",
                aclv.vote(party2, pm.applyTarget(permWriteResource1, resource1)).filter(Vote::allowed).isPresent());

        assertTrue("`party2:?:write:resource2`",
                aclv.vote(party2, pm.applyTarget(permWriteResource2, resource2)).isPresent());
        assertFalse("`party2:denied:write:resource2`",
                aclv.vote(party2, pm.applyTarget(permWriteResource2, resource2)).filter(Vote::denied).isPresent());

        assertTrue("`party2` inherits `anyone:?:write:anything`",
                aclv.vote(party2, permWriteAnything).filter(v -> v.sid().equals(anyone)).isPresent());

        assertFalse("`party2` inherits `anyone:?:write:resource2`",
                aclv.vote(party2, pm.applyTarget(permWriteResource2, resource2)).filter(v -> v.sid().equals(anyone)).isPresent());

    }
}
