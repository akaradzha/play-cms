/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.models;

import akka.actor.Cancellable;
import play.db.jpa.JPA;
import ch.insign.commons.db.ModelEvents;
import ch.insign.commons.db.ModelListener;
import ch.insign.commons.search.DefaultSearchResult;
import ch.insign.commons.search.SearchProvider;
import ch.insign.commons.search.SearchQuery;
import ch.insign.commons.search.SearchResult;
import com.github.kevinsawicki.http.HttpRequest;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.MultiFields;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopScoreDocCollector;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Bits;
import org.apache.lucene.util.Version;
import org.jsoup.Jsoup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.libs.Akka;
import play.mvc.Http;
import scala.concurrent.duration.Duration;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.TimeUnit;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * A default search provider for cms blocks.
 *
 * TODO: This is work in progress, it does not really search content yet
 */
public class LuceneSearchProvider implements SearchProvider  {
	private final static Logger logger = LoggerFactory.getLogger(LuceneSearchProvider.class);

    protected StandardAnalyzer analyzer = new StandardAnalyzer();

    private static final String FIELD_ID = "id";
    private static final String FIELD_TITLE = "title";
    private static final String FIELD_CONTENT = "content";
    private static final String INDEX_PATH = CMS.getConfig().luceneFolder();

    private static Queue<String> updateBacklog = new ConcurrentLinkedQueue<>();
    private static Queue<String> deleteBacklog = new ConcurrentLinkedQueue<>();
    private static Cancellable schedule;

    private final ModelListener listener = new ModelListener<PageBlock>() {
        @Override
        public void onPostUpdate(PageBlock page) {
            updateBacklog.add(page.getId());
            logger.debug("Lucene: Adding to index update backlog: " + page);
        }
        @Override
        public void onPostPersist(PageBlock page) {
            updateBacklog.add(page.getId());
            logger.debug("Lucene: Adding to index update backlog: " + page);
        }
        @Override
        public void onPreRemove(PageBlock page) {
            deleteBacklog.add(page.getId());
            logger.debug("Lucene: Adding to index remove backlog: " + page);
        }
    };


    @Override
    public void attach() {

        // Test if the Lucene folder is accessible, throw a runtime exception otherwise.
        try {
            Directory index = FSDirectory.open(java.nio.file.Paths.get(INDEX_PATH));
            IndexWriter writer = getWriter(index);
            logger.debug("Lucene directory '" + INDEX_PATH + "' found. Documents: " + writer.numDocs());
            writer.close();
            index.close();

        } catch (IOException e) {
            throw new RuntimeException("Lucene index error while trying to access the index: " + e.getMessage(), e);
        }

        ModelEvents.addListener(PageBlock.class, listener);

        // Schedule periodic index updates
        schedule = Akka.system().scheduler().schedule(
                Duration.create(2, TimeUnit.SECONDS),
                Duration.create(10, TimeUnit.SECONDS),
                new Runnable(){

                    @Override
                    public void run() {

                        createFakeHttpContext(); // FIXME: Is there a better way? Does this work for my thread only?
                        JPA.withTransaction(new Runnable() {
                            @Override
                            public void run() {
                                backgroundTask();
                            }
                        });
                    }
                },
                Akka.system().dispatcher()
        );
    }

    /**
     * Work on the modification backlog
     * TODO: persisted backlog storage..
     */
    private void backgroundTask() {

        // Limit the max. iems per Akka run

        for (int i = 0; i < 100; i++) {
            String id = deleteBacklog.poll();
            if (id == null) break;
            removeFromIndex(id);
        }

        for (int i = 0; i < 10; i++) {
            String id = updateBacklog.poll();
            if (id == null) break;
            updateIndex(id);
        }

    }

    @Override
    public void detach() {
        schedule.cancel();
        ModelEvents.removeListener(PageBlock.class, listener);
    }


    @Override
    public List<SearchResult> search(SearchQuery query, List<SearchResult> results) {

        Query q = null;
        try {
            q = new QueryParser(FIELD_CONTENT, analyzer).parse(query.query);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        // TODO: Language-specific search

        // Lucene search hits threshold - must be higher than actual requested hits, since
        // there will be drop-outs (no permission)
        int hitsPerPage = query.limit * 2;

        IndexReader reader = null;
        Directory index = null;
        try {
            index = FSDirectory.open(java.nio.file.Paths.get(INDEX_PATH));
            //TODO: fix indexReader after version update
           // reader = IndexReader.open(index);
        } catch (IOException e) {
            logger.error("Lucene index error while trying to access the index.", e);
            return results;
        }

        IndexSearcher searcher = new IndexSearcher(reader);
        TopScoreDocCollector collector = TopScoreDocCollector.create(hitsPerPage);
        try {
            searcher.search(q, collector);
        } catch (IOException e) {
            e.printStackTrace();
        }
        ScoreDoc[] hits = collector.topDocs().scoreDocs;


        logger.debug("LuceneSearchProvider: Found " + hits.length + " hits.");

        // Loop through the lucene results, fill up our results until the limit is reached (or we have no more results from lucene)
        int i = 0;
        while(results.size() < query.limit &&  i < hits.length) {
            int docId = hits[i].doc;
            i++;
            Document d = null;
            try {
                d = searcher.doc(docId);
                String blockId = d.get(FIELD_ID);

                // Remove those candidates the user has no access to
                // TODO: Better way, since an intermediate collection could also prevent reading..
                AbstractBlock block = AbstractBlock.find.byId(blockId);
                if (block == null) continue;

                if (!block.canRead()) {
                    logger.info("LuceneSearchProvider: Dropping block " + block + ": User has no permissions");
                    continue;
                }

                PageBlock page = block.getMyPage();
                if (!page.canRead()) {
                    logger.info("LuceneSearchProvider: Dropping page " + page + ": User has no permissions");
                    continue;
                }

                // Add to the results
                DefaultSearchResult result = new DefaultSearchResult();
                result
                    .setTitle(page.getPageTitle().get())
                    .setUrl(page.getNavItem().getURL())
                    .setSnippet("...");

                results.add(result);

            } catch (IOException e) {
                e.printStackTrace();
            }

        }

        try {
            reader.close();
            index.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return results;
    }


    /**
     * Call whenever a page is added or updated (also if its subblocks are modified).
     *
     * @param id the page id
     */
    @Override
    public void updateIndex(String id) {

        PageBlock page = PageBlock.find.byId(id);

        if (page == null || !page.isAvailableForSearch()) return;

        logger.debug("Lucene: Updating index for " + page);
        removeFromIndex(page.getId());

        String title = page.getPageTitle().get();

        // Fetch the content by a real request
        String url = CMS.getConfig().baseUrl() + page.getNavItem().getURL();
        HttpRequest request = HttpRequest.get(url);  // TODO: for all langs

        if (request.code() != 200) {
            logger.warn("Lucene: URL " + url + " returned a " + request.code() + ", ignoring this page.");
            return;
        }

        String content = request.body();
        org.jsoup.nodes.Document jsoupDoc = Jsoup.parse(content);

        String cleanContent = jsoupDoc.text();

        if (title.isEmpty() && cleanContent.isEmpty()) {
            return;
        }

        Document document = new Document();
        document.add(new StringField(FIELD_ID, page.getId(), Field.Store.YES));
        document.add(new TextField(FIELD_TITLE, title, Field.Store.YES));
        document.add(new TextField(FIELD_CONTENT, cleanContent, Field.Store.YES));

        try {
            Directory index = FSDirectory.open(java.nio.file.Paths.get(INDEX_PATH));
            IndexWriter writer = getWriter(index);
            writer.addDocument(document);
            writer.commit();
            writer.close();
            index.close();
            logger.info("Lucene: Added/updating url " + url);
        } catch (IOException e) {
            e.printStackTrace();
        }

        //debugOutputLuceneIndex();
    }

    /**
     * Remove pages from index
     * @param id the block id of the to be deleted page
     */
    @Override
    public void removeFromIndex(String id) {

        Query q = null;
        try {
            q = new QueryParser(FIELD_ID, analyzer)
                    .parse("+" + FIELD_ID + ":" + id);

        } catch (ParseException e) {
            e.printStackTrace();
            return;
        }

        try {
            Directory index = FSDirectory.open(java.nio.file.Paths.get(INDEX_PATH));
            IndexWriter writer = getWriter(index);
            writer.deleteDocuments(q);
            logger.debug("Lucene: Deleting '" + q + "'");
            writer.commit();
            writer.close();
            index.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        //debugOutputLuceneIndex();
    }

    /**
     * Empty and re-fill the search index
     */
    @Override
    public void rebuildIndex() {
        // TO DO
    }

    private IndexWriter getWriter(Directory index) throws IOException {
        IndexWriterConfig config = new IndexWriterConfig(analyzer);
        return new IndexWriter(index, config);
    }


    private IndexReader getReader(Directory index) throws IOException {
        return null;
        // FIXME after lucene version update
        //return IndexReader.open(index);
    }

    private void debugOutputLuceneIndex() {
//        try {
//            String[] list = index.listAll();
//            logger.debug("Lucene index - # of files: " + list.length);
//            for (int i = 0; i < list.length; i++) {
//                logger.debug(String.format("Lucene entry: %d. %s", i, list[i]));
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

        List<SearchResult> results = search(new SearchQuery("title:home"), new ArrayList<SearchResult>());

        try {
            Directory index = FSDirectory.open(java.nio.file.Paths.get(INDEX_PATH));
            IndexReader reader = getReader(index);
            String[] list = index.listAll();
            logger.debug("Lucene index - # of files: " + list.length);

            Bits liveDocs = MultiFields.getLiveDocs(reader);
            for (int i=0; i<reader.maxDoc(); i++) {
                if (liveDocs != null && !liveDocs.get(i)) {
                    continue;
                }
                Document doc = reader.document(i);
            }

            reader.close();
            index.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Create a fake http context which allows to access Http.context / Controller.ctx() from tests
     * Will return "testhost" when request.host() is accessed.
     *
     */
    public static void createFakeHttpContext() {

        // Create a mock http context  using Mockito
        Http.Request request = mock(Http.Request.class);
        when(request.host()).thenReturn("testhost"); // <-- FIXME: proper setting from where?
        // TODO: Add more mock returns as needed

        Map<String, String> flashData = Collections.emptyMap();
        Map<String, Object> argData = Collections.emptyMap();
        play.api.mvc.RequestHeader header = mock(play.api.mvc.RequestHeader.class);
        Http.Context context = new Http.Context(2L, header, request, flashData, flashData, argData);
        Http.Context.current.set(context);
    }
}
