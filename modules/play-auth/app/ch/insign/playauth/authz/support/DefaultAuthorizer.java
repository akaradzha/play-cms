/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.playauth.authz.support;

import ch.insign.playauth.authz.AccessControlListVoter;
import ch.insign.playauth.authz.Authorizer;
import ch.insign.playauth.authz.DomainPermission;
import ch.insign.playauth.permissions.GlobalDomainPermission;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;

public class DefaultAuthorizer implements Authorizer {

	private final static Logger logger = LoggerFactory.getLogger(DefaultAuthorizer.class);

	private final AccessControlListVoter aclVoter;

	@Inject
	public DefaultAuthorizer(AccessControlListVoter aclVoter) {
		this.aclVoter = aclVoter;
	}

	public boolean isPermitted(Object authority, DomainPermission<?> permission) {
		return aclVoter
				// the DomainPermission.ALL has highest priority
				.vote(authority, GlobalDomainPermission.ALL)
				.map(AccessControlListVoter.Vote::allowed)
				.orElseGet(() ->
					aclVoter
					.vote(authority, permission)
					.map(AccessControlListVoter.Vote::allowed)
					.orElse(false));
	}
}
