/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.models;

/**
 * Setup code, mostly used during initial setup or later resets.
 * Implement this interface to create you project-specific behaviour.
 */
public interface Bootstrapper {
    /**
     * Load essential data which is required for all configured sites to function properly.
     *
     * Used to perform an initial project bootstrap or to reload it to the initial state.
     */
    void loadEssentialData();

    /**
     * Load example data for all configured sites.
     */
    void loadExampleData();

    /**
     *  Load essential data which is required for the specified site to function properly.
     *
     *  Useful when adding a new site configuration.
     */
    void loadEssentialSiteData(Sites.Site site);

    /**
     * Load example data for the specified site.
     */
    void loadExampleSiteData(Sites.Site site);
}
