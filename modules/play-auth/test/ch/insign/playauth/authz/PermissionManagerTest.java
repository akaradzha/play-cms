package ch.insign.playauth.authz;

import ch.insign.playauth.authz.mock.AccessControlListMock;
import ch.insign.playauth.authz.mock.PermissionManagerProviderMock;
import ch.insign.playauth.authz.support.DefaultObjectIdentityRetrievalStrategy;
import ch.insign.playauth.authz.support.DefaultPermissionManager;
import org.junit.Before;
import org.junit.Test;
import play.Environment;
import play.inject.Injector;
import play.test.Helpers;

import javax.inject.Inject;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class PermissionManagerTest {

    @WithIdentifier(Domain1Idetifier.class)
    public interface Domain1 {
        String getId();
    }

    public static class Domain1Idetifier implements Identifier {
        @Override
        public Optional<String> getIdentifier(Object obj) {
            if (obj instanceof Domain1) {
                return Optional.of((Domain1) obj).map(Domain1::getId);
            }
            return Optional.empty();
        }
    }

    public interface Domain2 {
        String getId();
    }

    @Test
    public void definePermissionTest() {
        Injector injector = mock(Injector.class);
        when(injector.instanceOf(Domain1Idetifier.class)).thenReturn(new Domain1Idetifier());

        PermissionManagerProviderMock pmp = new PermissionManagerProviderMock();
        ObjectIdentityRetrievalStrategy oidrs = new DefaultObjectIdentityRetrievalStrategy(pmp, injector);
        AccessControlList acl = new AccessControlListMock();
        PermissionManager pm = pmp.set(new DefaultPermissionManager(acl, oidrs, null, Environment.simple())).get();

        assertTrue("There is no defined permissions initially.", pm.getDefinedPermissions().isEmpty());

        pm.definePermission(Domain1.class, "d1p1");
        pm.definePermission(Domain1.class, "d1p2");
        pm.definePermission(Domain2.class, "d2p1");

        assertEquals("There are three defined permissions", pm.getDefinedPermissions().size(), 3);

        assertEquals("There are two Domain1 permissions", pm.getDefinedPermissions(Domain1.class).size(), 2);
        assertEquals("There is one Domain2 permission", pm.getDefinedPermissions(Domain2.class).size(), 1);
    }


    @Test
    public void allowingRulesTest() {
        Injector injector = mock(Injector.class);
        when(injector.instanceOf(Domain1Idetifier.class)).thenReturn(new Domain1Idetifier());

        PermissionManagerProviderMock pmp = new PermissionManagerProviderMock();
        ObjectIdentityRetrievalStrategy oidrs = new DefaultObjectIdentityRetrievalStrategy(pmp, injector);
        AccessControlList acl = new AccessControlListMock();
        PermissionManager pm = pmp.set(new DefaultPermissionManager(acl, oidrs, null)).get();

        DomainPermission<Domain1> d1p1 = pm.definePermission(Domain1.class, "d1p1");
        DomainPermission<Domain1> d1p2 = pm.definePermission(Domain1.class, "d1p2");
        DomainPermission<Domain2> d2p1 = pm.definePermission(Domain2.class, "d2p1");

        pm.addAllowingRule(d1p1, d1p2);

        assertTrue("d1p1 allows d1p2", pm.allows(d1p1, d1p2));
        assertFalse("d1p2 does not allow d1p1", pm.allows(d1p2, d1p1));
        assertFalse("d1p1 does not allow d2p1", pm.allows(d1p1, d2p1));
    }

    @Test
    public void impliesTest() {
        Injector injector = mock(Injector.class);
        when(injector.instanceOf(Domain1Idetifier.class)).thenReturn(new Domain1Idetifier());

        PermissionManagerProviderMock pmp = new PermissionManagerProviderMock();
        ObjectIdentityRetrievalStrategy oidrs = new DefaultObjectIdentityRetrievalStrategy(pmp, injector);
        AccessControlList acl = new AccessControlListMock();
        PermissionManager pm = pmp.set(new DefaultPermissionManager(acl, oidrs, null)).get();

        //FIXME: play-auth does not handle lambda's properly
        Domain1 instance1 = () -> "d1i1";
        Domain1 instance2 = () -> "d1i2";

        // p1All is the most general permission
        DomainPermission<Domain1> p1All = pm.definePermission(Domain1.class, "p1");

        // p1Class is bound to the specific domain subclass
        DomainPermission<Domain1> p1Class = pm.applyTarget(p1All, Domain1.class);

        // p1Instance1 and p1Instance2 are bound to specific domain instances and are the most specific
        DomainPermission<Domain1> p1Instance1 = pm.applyTarget(p1All, instance1);
        DomainPermission<Domain1> p1Instance2 = pm.applyTarget(p1All, instance2);

        assertTrue(pm.implies(p1All, p1All));
        assertTrue(pm.implies(p1All, p1Class));
        assertTrue(pm.implies(p1All, p1Instance1));
        assertTrue(pm.implies(p1All, p1Instance2));
        assertTrue(pm.implies(p1Class, p1Class));

        assertTrue(pm.implies(p1Class, p1Instance1));
        assertTrue(pm.implies(p1Class, p1Instance2));

        assertTrue(pm.implies(p1Instance1, p1Instance1));
        assertTrue(pm.implies(p1Instance2, p1Instance2));

        assertFalse(pm.implies(p1Class, p1All));
        assertFalse(pm.implies(p1Instance1, p1All));
        assertFalse(pm.implies(p1Instance1, p1Class));
        assertFalse(pm.implies(p1Instance1, p1Instance2));
        assertFalse(pm.implies(p1Instance2, p1All));
        assertFalse(pm.implies(p1Instance2, p1Class));
        assertFalse(pm.implies(p1Instance2, p1Instance1));
    }
}
