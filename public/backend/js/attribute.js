jQuery(function ($) {

    initDeleteAttribute();

    function initDeleteAttribute() {
        var $modal = $('#modal-confirm-delete-attribute');

        $('.delete-attribute').click(function (e) {
            e.preventDefault();
            var formId = $(this).parents('form').attr("id");
            $modal.data('form-id', formId).modal('show');
        });

        $('#confirm-attribute-delete').click(function (e) {
            e.preventDefault();
            $("#" + $modal.data('form-id')).submit();
        });
    }
});
