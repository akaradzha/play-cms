import ch.insign.commons.db.Model;
import ch.insign.commons.db.util.TestDatabase;
import ch.insign.commons.db.util.TestTask;
import ch.insign.commons.util.TaskQueue;
import com.google.inject.AbstractModule;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import play.Application;
import play.Mode;
import play.inject.guice.GuiceApplicationBuilder;
import play.test.Helpers;
import play.test.WithApplication;

import java.time.Instant;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static play.test.Helpers.inMemoryDatabase;

public class TaskQueueTest extends WithApplication {

    @Rule
    public TestDatabase db = new TestDatabase();

    @Override
    @SuppressWarnings("unchecked")
    public Application provideApplication() {
        return new GuiceApplicationBuilder()// Create a guice application with modules
                .in(Helpers.class.getClassLoader())// Take a class loader provided by Play! helper
                .in(Mode.TEST)// Set an environment mode for configuration
                .configure((Map) inMemoryDatabase())// Explicitly convert to Map, not Configuration
//                .bindings(new AbstractModule(){
//                    @Override
//                    protected void configure() {
//                        requestStaticInjection(Model.class);
//                    }
//                })// Explicitly convert to Map, not Configuration
                .build();
    }

    @Before
    public void init() {
        Model.setJpaApi(db.jpa);
        TaskQueue.setJpaApi(db.jpa);
    }

    @Test
    public void testPersistence() {
        String queueName = "testPersistence";
        TaskQueue queue = new TaskQueue(queueName);

        db.jpa.withTransaction(() -> {
            TestTask.completionOrder.set("");
            TestTask task1 = new TestTask();
            TestTask task2 = new TestTask();
            TestTask task3 = new TestTask();

            task1.setTag("1");
            task2.setTag("2");
            task3.setTag("3");

            queue.add(task1);
            queue.add(task2);
            queue.add(task3);

            queue.start();
            queue.process();

            TaskQueue queue2 = new TaskQueue(queueName).load().start();

            assertEquals("Queue size after loading doesn't equals to 2", 2, queue2.size());

            int iterations = 0;
            while (queue2.size() > 0 && iterations < 100) {
                queue2.process();
                iterations++;
            }
            assertEquals("Incorrect step count", 2, iterations);
            assertEquals("Queue isn't empty", 0, queue2.size());
            assertEquals("Incorrect order", "123", TestTask.completionOrder.get());
            TestTask.completionOrder.set("");
        });
    }

    @Test
    public void testPriority() {
        TaskQueue queue = new TaskQueue("testPriority").start();

        db.jpa.withTransaction(() -> {
            TestTask.completionOrder.set("");
            TestTask task1 = new TestTask();
            TestTask task2 = new TestTask();
            TestTask task3 = new TestTask();

            task1.setTag("a");
            task2.setTag("b");
            task3.setTag("c");

            task1.setPriority(3);
            task2.setPriority(2);
            task3.setPriority(1);
            task3.setFailTimes(1);

            queue.add(task1);
            queue.add(task2);
            queue.add(task3);

            int iterations = 0;
            while (queue.size() > 0 && iterations < 100) {
                queue.process();
                iterations++;
            }

            assertEquals("Incorrect step count", 4, iterations);
            assertEquals("Queue isn't empty", 0, queue.size());
            assertEquals("Incorrect order", "cba", TestTask.completionOrder.get());
            TestTask.completionOrder.set("");
        });
    }

    @Test
    public void testScheduling() {
        TaskQueue queue = new TaskQueue("testScheduling").start();

        db.jpa.withTransaction(() -> {
            TestTask.completionOrder.set("");
            TestTask task1 = new TestTask();
            TestTask task2 = new TestTask();
            TestTask task3 = new TestTask();
            TestTask task4 = new TestTask();

            task1.setTag("A");
            task2.setTag("B");
            task3.setTag("C");
            task4.setTag("D");

            task1.setScheduleAfter(Instant.now().plusSeconds(1));
            task2.setScheduleAfter(Instant.now().plusSeconds(2));
            task3.setScheduleAfter(Instant.now().plusSeconds(2));
            task4.setScheduleAfter(Instant.now().plusSeconds(1));

            task1.setPriority(3);
            task2.setPriority(2);
            task3.setPriority(1);
            task4.setPriority(0);

            // So far: dacb
            task4.setFailTimes(1);
            task4.setRetryWaitPeriod(3);

            // Now : acbd
            queue.add(task1);
            queue.add(task2);
            queue.add(task3);
            queue.add(task4);

            int iterations = 0;
            while (queue.size() > 0 && iterations < 150) {
                try {
                    queue.process();
                    iterations++;

                    Thread.sleep(50);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    fail();
                }
            }

            assertEquals("Queue isn't empty", 0, queue.size());
            assertEquals("Incorrect order", "ACBD", TestTask.completionOrder.get());
            TestTask.completionOrder.set("");
        });
    }

}
