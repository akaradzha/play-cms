/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.permissions;

import ch.insign.cms.models.AbstractBlock;
import ch.insign.playauth.authz.Authorizer;
import ch.insign.playauth.authz.DomainPermission;
import ch.insign.playauth.authz.PermissionManager;

import java.util.stream.Stream;

import static ch.insign.playauth.PlayAuth.getAuthorizer;
import static ch.insign.playauth.PlayAuth.getPermissionManager;

/**
 * Provides custom authorization behavior for {@code AbstractBlock} objects
 * */
public class BlockAuthorizer implements Authorizer {

	@Override
	public boolean isPermitted(Object authority, DomainPermission<?> permission) {

		// delegate READ & MODIFY checks to the permission.target() if it is instance

		// delegate permission check to AbstractBlock.canRead/canModify if:
		// a) permission.target() represents AbstractBlock instance (it may also represent a class);
		// b) if permission is READ or MODIFY (underlying project may add more permissions for AbstractBlock).
		if (supports(permission)) {

			AbstractBlock target = AbstractBlock.find.byId(permission.target().getIdentifier());

			if (target != null) {
				if (BlockPermission.READ.name().equals(permission.name())) {
					return target.canRead(authority);
				}

				if (BlockPermission.MODIFY.name().equals(permission.name())) {
					return target.canModify(authority);
				}
			}
		}

		// delegate skipped permission checks to PlayAuth authorizer
		return getAuthorizer().isPermitted(authority, permission);
	}

	private boolean supports(DomainPermission<?> permission) {
		if (permission.target().isClassIdentity()) {
			// only instance-level checks are supported by AbstractBlock
			return false;
		}

		PermissionManager pm = getPermissionManager();
		String permName = pm.getQualifiedName(permission);

		// only BlockPermission values are supported by AbstractBlock
		return Stream.of(BlockPermission.values())
				.map(pm::getQualifiedName)
				.anyMatch(p -> p.equals(permName));
	}
}
