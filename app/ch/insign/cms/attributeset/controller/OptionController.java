/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.attributeset.controller;

import ch.insign.cms.attributeset.model.Attribute;
import ch.insign.cms.attributeset.model.Option;
import ch.insign.cms.attributeset.model.OptionAttribute;
import ch.insign.cms.permissions.aop.RequiresBackendAccess;
import ch.insign.commons.db.SecureForm;
import ch.insign.commons.db.SmartForm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.data.Form;
import play.db.jpa.Transactional;
import play.mvc.Controller;
import play.mvc.Result;
import ch.insign.cms.attributeset.views.html.backend.optionAddEdit;

@RequiresBackendAccess
@Transactional
public class OptionController extends Controller {

    private final static Logger logger = LoggerFactory.getLogger(OptionController.class);

    public Result showAdd(String attrId) {
        Attribute attribute = Attribute.find.byId(attrId);
        Form<Option> form = SmartForm.form(Option.class);
        return ok(SecureForm.signForms(optionAddEdit.render(form, attribute, null)));
    }

    public Result doAdd(String attrId) {
        OptionAttribute attribute = (OptionAttribute) Attribute.find.byId(attrId);

        Form<Option> form = SmartForm.form(Option.class).bindFromRequest();

        if (form.hasErrors()) {
            return badRequest(SecureForm.signForms(optionAddEdit.render(form, attribute, null)));
        }

        Option option = form.get();
        option.setParentAttribute(attribute);
        attribute.getOptions().add(option);
        attribute.save();

        return redirect(ch.insign.cms.attributeset.controller.routes.AttributeController.showEdit(attrId));
    }

    public Result showEdit(String optionId, String attributeId){
        Option option = Option.find.byId(optionId);
        Attribute attribute = Attribute.find.byId(attributeId);
        if (option == null){
            throw new IllegalArgumentException();
        }

        Form<Option> form = SmartForm.form(Option.class).fill(option);
        return ok(SecureForm.signForms(optionAddEdit.render(form, attribute, option)));
    }

    public Result doEdit(String optionId, String attributeId){
        Option option = Option.find.byId(optionId);
        Form<Option> form = SmartForm.form(Option.class).fill(option).bindFromRequest();
        form.get().save();
        return redirect(ch.insign.cms.attributeset.controller.routes.AttributeController.showEdit(attributeId));
    }

    public Result delete(String optionId, String attributeId) {
        Option option = Option.find.byId(optionId);
        option.delete();
        return redirect(ch.insign.cms.attributeset.controller.routes.AttributeController.showEdit(attributeId));
    }
}
