/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.playauth.party.preference;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import play.libs.Json;

import javax.persistence.Embeddable;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Transient;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Embeddable
public class Preference {

    private String name;
    private String value;

    @Transient
    private List<String> _options = new ArrayList<>();

    protected Preference() {

    }

    public Preference(String name, String option) {
        this.name = name;
        this._options.add(option);
        updateValue();
    }

    public String getName() {
        return name;
    }

    public List<String> getOptions() {
        return new ArrayList<>(_options());
    }

    public Optional<String> getFirstOption() {
        return _options().stream().findFirst();
    }

    public Preference appendOption(String option) {
        _options().remove(option);
        _options().add(option);
        updateValue();

        return this;
    }

    public Preference prependOption(String option) {
        _options().remove(option);
        _options().add(0, option);
        updateValue();

        return this;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 31)
                .append(name)
                .toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }

        Preference other = (Preference) obj;
        return new EqualsBuilder()
                .append(name, other.name)
                .append(value, other.value)
                .isEquals();
    }

    private List<String> _options() {
        if ((_options == null || _options.isEmpty()) && value != null) {
            _options = Json.fromJson(Json.parse(value), List.class);
        }

        return _options;
    }

    private void updateValue() {
        value = Json.toJson(_options).toString();
    }

    @Override
    public String toString() {
        return name + " = " + value;
    }
}
