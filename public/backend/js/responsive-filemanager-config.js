$(document).ready(function ($) {
    // Adjust frame options for file manager using fancybox
    $('.iframe-btn').fancybox({
        fitToView: false,
        autoSize: false,
        type: 'iframe',
        afterLoad: function () {
            this.width = $(this.element).data('width');
            this.height = $(this.element).data('height');
        }
    });

});

/**
 * Responsive filemanager call-back function, immediately invokes a block of code when the field is updated.
 * Should always be accessible and included to global scope as far as we use FM on php language.
 * For more info {@see http://www.responsivefilemanager.com/index.php}
 * - documentation with some use cases according to this method
 *
 * @param field_id - id of updated field
 */
function responsive_filemanager_callback(field_id) {
    var field = document.getElementById(field_id);
    // Add prefix to URL to improve relative url retrieving
    field.value = '/browse/source/' + field.value;
}