/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.playauth.authz.support;

import ch.insign.playauth.authz.*;
import ch.insign.playauth.utils.CacheUtils;
import com.google.common.collect.AbstractIterator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.db.jpa.JPAApi;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.TypedQuery;
import java.io.Serializable;
import java.util.*;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static ch.insign.playauth.utils.CacheUtils.cached;

public class DefaultAccessControlList implements AccessControlList {

	private final static Logger logger = LoggerFactory.getLogger(AccessControlList.class);
	private final static String CACHE_NAME = AccessControlList.class.getName();

	private JPAApi jpaApi;

	@Inject
	public DefaultAccessControlList(JPAApi jpaApi) {
		this.jpaApi = jpaApi;
	}

	@Override public Optional<AccessControlEntry> find(SecurityIdentity sid, ObjectIdentity oid) {
	    return cached(CACHE_NAME, new AccessControlListCacheKey(sid, oid), () -> find(new AccessControlEntry.Identifier(sid, oid)));
    }

    @Override public Optional<AccessControlEntry> find(AccessControlEntry.Identifier id) {
        return Optional.ofNullable(em().find(AccessControlEntry.class, id));
    }

    @Override public List<AccessControlEntry> findAll() {
        return query("findAll").getResultList();
    }

	@Override public Stream<AccessControlEntry> streamAll() {
		return StreamSupport.stream(new AccessControlListIterator(em()).spliterator(), false);
	}

    @Override public List<AccessControlEntry> findBySid(SecurityIdentity sid) {
	    return cached(CACHE_NAME, new AccessControlListCacheKey(sid), () -> query("findBySid")
			    .setParameter("type", sid.getType())
			    .setParameter("identifier", sid.getIdentifier())
			    .getResultList());
    }

    @Override public List<AccessControlEntry> findByOid(ObjectIdentity oid) {
	    return cached(CACHE_NAME, new AccessControlListCacheKey(oid), () -> query("findByOid")
				    .setParameter("type", oid.getType())
				    .setParameter("identifier", oid.getIdentifier())
				    .getResultList());
    }

    @Override public boolean contains(AccessControlEntry ace) {
        return em().contains(ace) || find(ace.getId()).isPresent();
    }

    /**
     * Put a given ACE to the ACL. An existing entry will be overriden.
     */
    @Override public AccessControlEntry put(AccessControlEntry ace) {
        if (contains(ace)) {
            ace = em().merge(ace);
        } else {
            em().persist(ace);
        }

	    CacheUtils.update(CACHE_NAME, new AccessControlListCacheKey(ace.getSid(), ace.getOid()), Optional.of(ace));
	    CacheUtils.remove(CACHE_NAME, new AccessControlListCacheKey(ace.getSid()));
	    CacheUtils.remove(CACHE_NAME, new AccessControlListCacheKey(ace.getOid()));

        return ace;
    }

    @Override public void remove(AccessControlEntry ace) {
        if (contains(ace)) {
            remove(ace.getId());
        }
    }

    @Override public void remove(AccessControlEntry.Identifier id) {
        try {
            AccessControlEntry ace = em().getReference(AccessControlEntry.class, id);
            em().remove(ace);

	        CacheUtils.remove(CACHE_NAME, new AccessControlListCacheKey(ace.getSid(), ace.getOid()));
	        CacheUtils.remove(CACHE_NAME, new AccessControlListCacheKey(ace.getSid()));
	        CacheUtils.remove(CACHE_NAME, new AccessControlListCacheKey(ace.getOid()));

        } catch (EntityNotFoundException e) {
            logger.warn("Failed to remove AccessControlEntry(" + id.toString() + ")");
        }
    }

    private TypedQuery<AccessControlEntry> query(String name) {
        return em().createNamedQuery(
                AccessControlEntry.class.getSimpleName() + "." + name,
                AccessControlEntry.class);
    }

    private EntityManager em() {
        return jpaApi.em();
    }

	public static class AccessControlListCacheKey implements Serializable {
		public final SecurityIdentity sid;
		public final ObjectIdentity oid;

		AccessControlListCacheKey(SecurityIdentity sid, ObjectIdentity oid) {
			this.sid = sid;
			this.oid = oid;
		}

		AccessControlListCacheKey(SecurityIdentity sid) {
			this(sid, null);
		}

		AccessControlListCacheKey(ObjectIdentity oid) {
			this(null, oid);
		}

		@Override
		public boolean equals(Object o) {
			if (this == o) return true;
			if (o == null || getClass() != o.getClass()) return false;

			AccessControlListCacheKey that = (AccessControlListCacheKey) o;

			if (oid != null ? !oid.equals(that.oid) : that.oid != null) return false;
			if (sid != null ? !sid.equals(that.sid) : that.sid != null) return false;

			return true;
		}

		@Override
		public int hashCode() {
			int result = sid != null ? sid.hashCode() : 0;
			result = 31 * result + (oid != null ? oid.hashCode() : 0);
			return result;
		}
	}


	private static class AccessControlListIterator extends AbstractIterator<AccessControlEntry> {
		private static int CHUNK_SIZE = 400;
		private static String COUNT_ALL_QUERY_NAME = AccessControlEntry.class.getSimpleName() + "." + "countAll";
		private static String FIND_ALL_QUERY_NAME = AccessControlEntry.class.getSimpleName() + "." + "findAll";

		private final Long count;
		private final EntityManager em;
		private Iterator<AccessControlEntry> chunk;
		private long index = 0;

		AccessControlListIterator(EntityManager em) {
			super();
			this.em = em;
			this.count = em.createNamedQuery(COUNT_ALL_QUERY_NAME, Long.class).getSingleResult();
		}

		Spliterator<AccessControlEntry> spliterator() {
			return Spliterators.spliteratorUnknownSize(this, Spliterator.NONNULL);
		}

		private Iterator<AccessControlEntry> getChunk(long index, int chunkSize) {
			return em.createNamedQuery(FIND_ALL_QUERY_NAME, AccessControlEntry.class)
					.setFirstResult((int) index)
					.setMaxResults(chunkSize)
					.getResultList()
					.iterator();
		}

		@Override
		protected AccessControlEntry computeNext() {
			if (count == 0) {
				return endOfData();
			}

			if (chunk != null && !chunk.hasNext() && index >= count) {
				return endOfData();
			}

			if (chunk == null || !chunk.hasNext()) {
				chunk = getChunk(index, CHUNK_SIZE);
				index += CHUNK_SIZE;
			}

			if (chunk == null || !chunk.hasNext()) {
				return endOfData();
			}

			return chunk.next();
		}

	}
}
