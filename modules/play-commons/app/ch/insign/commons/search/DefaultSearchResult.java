/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.commons.search;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

/**
 * A default implementation of the SearchResult interface
 */
public class DefaultSearchResult implements SearchResult  {
	private final static Logger logger = LoggerFactory.getLogger(DefaultSearchResult.class);

    private String title;
    private String snippet;
    private String url;

    private String language;
    private String image;
    private String author;

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public String getSnippet() {
        return snippet;
    }

    @Override
    public String getUrl() {
        return url;
    }

    public DefaultSearchResult setTitle(String title) {
        this.title = title;
        return this;
    }

    public DefaultSearchResult setUrl(String url) {
        this.url = url;
        return this;
    }

    public DefaultSearchResult setSnippet(String snippet) {
        this.snippet = snippet;
        return this;
    }

    @Override
    public String getLanguage() {
        return language;
    }

    public DefaultSearchResult setLanguage(String language) {
        this.language = language;
        return this;
    }

    @Override
    public String getImage() {
        return image;
    }

    public DefaultSearchResult setImage(String image) {
        this.image = image;
        return this;
    }

    @Override
    public String getAuthor() {
        return author;
    }

    public DefaultSearchResult setAuthor(String author) {
        this.author = author;
        return this;
    }
}
