/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.models;

import ch.insign.cms.controllers.CmsContext;
import ch.insign.commons.filter.ContentFilter;
import ch.insign.commons.filter.Filterable;
import play.twirl.api.Html;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by anton on 23.09.14.
 *
 * Simple content filter  that renders tag only in frontend
 */
public abstract class FrontendWidget extends ContentFilter {

    @Override
    public String processTagOutput(String tag, List<String> params, Filterable source) {
        // do not render content in backEnd
        if (CmsContext.current() !=  null
                && (
                CmsContext.current().getAction() == CmsContext.Action.EDIT
                        || CmsContext.current().getAction() == CmsContext.Action.SAVE )) {
            return null;

        }

        return render(tag, params, source).toString();
    }

    public abstract Html render(String tag, List<String> params, Filterable source);

    /**
     * @return Map of all frontend widgets with tag as key and FrontendWidget as Value
     */
    public static HashMap<String, FrontendWidget> getAllWidgets() {
        HashMap<String, FrontendWidget> widgets = new HashMap<>();

        for (Map.Entry<String, ContentFilter> entry: CMS.getFilterManager().getTagFilters().entrySet()) {
            if (entry.getValue() instanceof FrontendWidget) {
                widgets.put(entry.getKey(), (FrontendWidget) entry.getValue());
            }
        }
        return widgets;
    }
}
