/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.models;


import akka.actor.ActorSystem;
import javax.inject.Inject;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import akka.actor.Cancellable;
import ch.insign.cms.events.ResetEvent;
import ch.insign.commons.db.ModelEvents;
import ch.insign.commons.db.ModelListener;
import ch.insign.commons.search.DefaultSearchResult;
import ch.insign.commons.search.SearchProvider;
import ch.insign.commons.search.SearchQuery;
import ch.insign.commons.search.SearchResult;
import ch.insign.commons.util.Configuration;
import com.github.kevinsawicki.http.HttpRequest;
import org.elasticsearch.ElasticsearchException;
import org.elasticsearch.action.admin.indices.create.CreateIndexResponse;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.admin.indices.exists.indices.IndicesExistsResponse;
import org.elasticsearch.action.count.CountResponse;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.common.text.Text;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.node.Node;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.db.jpa.JPAApi;
import play.libs.Akka;
import play.mvc.Http;
import scala.concurrent.duration.Duration;

import java.net.InetAddress;
import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.TimeUnit;

import static org.elasticsearch.client.Requests.createIndexRequest;
import static org.elasticsearch.node.NodeBuilder.nodeBuilder;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Use ElasticSearch as search engine backend.
 *
 */
public class ElasticSearchProvider implements SearchProvider  {
	private final static Logger logger = LoggerFactory.getLogger(ElasticSearchProvider.class);


    private boolean backgroundTaskEnabled = true;

    protected static JPAApi jpaApi;
    protected static ActorSystem actorSystem;

    public enum Field {
        pageId, title, url, description, keywords, content, language, image, displayFrom, displayTo,
        modified, pageClass, classHierarchy, author
    }

    protected class ElasticConfiguration extends Configuration  {
        @Override
        protected String configNamespace() {
            return "elastic";
        }

        @Override
        protected void test() {
            host();
            cluster();
            snippetSize();
            snippetNumber();
        }

        public String host() {
            return getConfig().getString("host");
        }

        public Integer port() {
            return getConfig().getInt("port");
        }

        public String cluster() {
            return getConfig().getString("cluster");
        }

        public int snippetSize() {
            return getConfig().getInt("snippet.size");
        }

        public int snippetNumber() {
            return getConfig().getInt("snippet.number");
        }

        // These are not (yet) configurable (but can be overwritten in tests)

        public String index() {
            return "cms";
        }

        public int delayStart() {
            return 5;
        }

        public int delayPeriod() {
            return 5;
        }
    }

    protected ElasticConfiguration config = new ElasticConfiguration();

    protected static Node node;
    protected static Client client;
    protected static ResetEvent.ResetListener resetListener;

    protected static Queue<String> updateBacklog = new ConcurrentLinkedQueue<>();
    protected static Queue<String> deleteBacklog = new ConcurrentLinkedQueue<>();
    protected static Cancellable schedule;


    private final ModelListener listener = new ModelListener<PageBlock>() {
        @Override
        public void onPostUpdate(PageBlock page) {
            if (!updateBacklog.contains(page.getId())) {
                logger.debug("Elastic: Adding to index update backlog: " + page);

                if (page.isAvailableForSearch()) {
                    updateBacklog.add(page.getId());

                } else {
                    // Ensure non-searchable pages are removed if they were in the index previously
                    onPreRemove(page);
                }

            }
        }

        @Override
        public void onPostPersist(PageBlock page) {
            if (!updateBacklog.contains(page.getId())) {
                logger.debug("Elastic: Adding to index update backlog: " + page);
                updateBacklog.add(page.getId());
            }
        }

        @Override
        public void onPreRemove(PageBlock page) {
            for (NavigationItem navItem : page.getNavItems().values()) {
                if (!deleteBacklog.contains(navItem.getId())) {
                    deleteBacklog.add(navItem.getId());
                }
            }
            logger.debug("Elastic: Adding to index remove backlog: " + page);

        }
    };

    @Inject
    public ElasticSearchProvider(JPAApi jpaApi, ActorSystem actorSystem) {
        this.jpaApi = jpaApi;
        this.actorSystem = actorSystem;
    }

    @Override
    public void attach() {

        // Test if the ElasticSearch node is accessible, throw a runtime exception otherwise.
        try {
            logger.info(String.format("Elastic: Trying to connect to cluster '%s' on '%s'", config.host(), config.cluster()));

            Settings settings = Settings.settingsBuilder()
                    .put("cluster.name", config.cluster()).build();
            client = TransportClient.builder().settings(settings).build()
                    .addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName(config.host()), config.port()));

//            node = nodeBuilder()
//                    .clusterName(config.cluster())
//                    .client(true)
//                    .node();
//            client = node.client();

            IndicesExistsResponse exists = client.admin().indices().prepareExists(config.index()).execute().actionGet();

            if (!exists.isExists()) {
                CreateIndexResponse createResponse = client.admin().indices().create(createIndexRequest(config.index())).actionGet();
                logger.info("Elastic: Created new index " + config.index() + ". Acknowledged: " + createResponse.isAcknowledged());
            }



        } catch (Exception e) {
            throw new RuntimeException("ElasticSearch: error while trying to create a client: " + e.getMessage(), e);
        }

        ModelEvents.addListener(PageBlock.class, listener);

        // Delete the index when resetting
        ResetEvent.addListener(new ResetEvent.ResetListener() {

            @Override
            public void onBeforeReset() {

                // Pause index updates and drop the index
                backgroundTaskEnabled = false;
                emptyIndex();
            }

            @Override
            public void onAfterReset() {

                // Empty the delete backlog only (as we've deleted the cms index entirely)
                deleteBacklog.clear();

                // Continue index updates after reset
                backgroundTaskEnabled = true;
            }
        });


        schedule = actorSystem.scheduler().schedule(
                Duration.create(config.delayStart(), TimeUnit.SECONDS),
                Duration.create(config.delayPeriod(), TimeUnit.SECONDS),
                () -> {
                    jpaApi.withTransaction(this::backgroundTask);
                },
                actorSystem.dispatcher()
        );
    }

    private void emptyIndex() {
        // Pause index updates during reset
        backgroundTaskEnabled = false;

        try {

            // Delete index
            client.admin().indices().delete(new DeleteIndexRequest(config.index())).actionGet();

            // Wait now for the yellow (or green) status
            client.admin().cluster().prepareHealth().setWaitForYellowStatus().execute().actionGet();
            logger.info("Elastic: Index '" + config.index() + "' deleted.");

        } catch (ElasticsearchException e) {
            e.printStackTrace();
        }
    }

    /**
     * Work on the modification backlog
     */
    private void backgroundTask() {

        try {
            // Limit the max. iems per Akka run
            for (int i = 0; i < 100; i++) {
                if (!backgroundTaskEnabled) return;
                String id = deleteBacklog.poll();
                if (id == null) break;
                removeFromIndex(id);
            }

            for (int i = 0; i < 10; i++) {
                if (!backgroundTaskEnabled) return;
                String id = updateBacklog.poll();
                if (id == null) break;
                updateIndex(id);
            }

        } catch (Exception e) {

            logger.warn("Error while indexing: " + e.getMessage());
            e.printStackTrace();
        }
    }


    @Override
    public void detach() {
        schedule.cancel();
        ModelEvents.removeListener(PageBlock.class, listener);
        client.close();
    }

    /**
     * Empty the ElasticSearch index, then add all (searchable) pages to the update backlog.
     */
    public void rebuildIndex() {

        logger.info("ElasticSearch: Starting index rebuild...");
        backgroundTaskEnabled = false;

        updateBacklog.clear();
        deleteBacklog.clear();

        List<PageBlock> pages = PageBlock.find.all();

        emptyIndex();

        for (PageBlock page : pages) {
            if (page.isAvailableForSearch()) {
                updateBacklog.add(page.getId());
            }
        }

        logger.info("ElasticSearch: Added " + updateBacklog.size() + " searchable pages to the update backlog.");

        backgroundTaskEnabled = true;

    }

    public String buildElasticQueryString(SearchQuery query) {
        return String.format("+%s:%s +(%s)",
                Field.language,
                query.language,
                query.query
        );
    }

    public SearchRequestBuilder buildSearchRequest(SearchQuery query) {

        String queryString = buildElasticQueryString(query);
        logger.debug("Elasic query string: " + queryString);
        QueryBuilder qb = QueryBuilders.queryStringQuery(queryString);

        return client.prepareSearch(config.index())
                .setTypes("page")
                .setQuery(qb)
                .addHighlightedField(
                        Field.content.toString(),
                        config.snippetSize(),
                        config.snippetNumber())
                .setSize(query.limit);
    }

    /**
     * Get the result count for the given query
     */
    public long countResults(SearchQuery query) {

        String queryString = buildElasticQueryString(query);
        QueryBuilder qb = QueryBuilders.queryStringQuery(queryString);

        CountResponse response = client.prepareCount(config.index())
                .setQuery(qb)
                .execute()
                .actionGet();

        return response.getCount();
    }

    /**
     * Creates the snippet out of the ElasticSearch hit
     */
    public String buildSnippet(SearchHit hit) {
        String snippet = "";

        if (hit.highlightFields().get(Field.content.toString()) != null) {

            Text[] fragments = hit.highlightFields().get(Field.content.toString()).fragments();
            StringBuilder sb = new StringBuilder();

            // Concat snippet lines together - currently just 2, tbd.
            for (int j = 0; j < Math.min(fragments.length, 3); j++) {
                sb.append(fragments[j].toString());
                sb.append(".");
            }
            snippet = sb.toString();
        }

        return onBuildSnippet(hit, snippet);
    }

    /**
     * Override this method to modify the generated snippet.
     * Hint: The doc id in the hit is the NavigationItem's id
     *
     * @param hit the ElasticSearch hit
     * @param snippet the default generated snippet
     * @return the snippet to be used
     */
    protected String onBuildSnippet(SearchHit hit, String snippet) {
        return snippet;
    }


    @Override
    public List<SearchResult> search(SearchQuery query, List<SearchResult> results) {

        try {
            SearchRequestBuilder builder = buildSearchRequest(query);

            SearchResponse response = builder.execute().actionGet();

            SearchHits hits = response.getHits();
            logger.debug("ElasticSearchProvider: Found " + hits.totalHits() + " hits.");

            // Loop through the lucene results, fill up our results until the limit is reached (or we have no more results from lucene)
            int i = 0;
            while(results.size() < query.limit &&  i < hits.getTotalHits()) {

                SearchHit hit = hits.getAt(i);
                String blockId = (String) hit.getSource().get(Field.pageId.toString());
                String hitlang = (String) hit.getSource().get(Field.language.toString());
                String image = (String) hit.getSource().get(Field.image.toString());
                String author = (String) hit.getSource().get(Field.author.toString());
                i++;

                // Remove those candidates the user has no access to
                // TODO: Better way, since an intermediate collection could also prevent reading..
                AbstractBlock block = AbstractBlock.find.byId(blockId);
                if (block == null) continue;

                if (!block.canRead()) {
                    logger.info("ElasticSearchProvider: Dropping block " + block + ": User has no permissions");
                    continue;
                }
				//If the user does not have the right to change the block, the block should be checked for a time restriction
	            if (!block.canModify() && !block.checkTimeRestriction()) {
		            logger.info("ElasticSearchProvider: Restriction block stop" + block + ": For anonymous");
		            continue;
	            }

                PageBlock page = block.getMyPage();
                if (!page.canRead()) {
                    logger.info("ElasticSearchProvider: Dropping page " + page + ": User has no permissions");
                    continue;
                }

                if (!page.getNavItem(hitlang).isVisible()) {
                    logger.info("ElasticSearchProvider: Dropping page " + page + ": navigation item not visible");
                    continue;
                }

                // Add to the results
                DefaultSearchResult result = new DefaultSearchResult();
                result
                        .setTitle(page.getPageTitle().get(hitlang))
                        .setSnippet(buildSnippet(hit))
                        .setUrl(page.getNavItem(hitlang).getURL())
                        .setLanguage(hitlang)
                        .setAuthor(author)
                        .setImage(image);

                results.add(result);

            }

        } catch (Exception e) {
            logger.error("ElasticSearch: index error while trying to access the index.", e);
            e.printStackTrace();
            return results;
        }

        return results;
    }


    /**
     * Call whenever a page is added or updated (also if its subblocks are modified).
     *
     * @param id the page id
     */
    @Override
    public void updateIndex(String id) {

        PageBlock page = PageBlock.find.byId(id);

        if (page == null || !page.isAvailableForSearch()) return;

        // Process all language versions
        for (NavigationItem navItem : page.getNavItems().values()) {

            if (!navItem.isVisible()) continue;
            String lang = navItem.getLanguage();

            String title = page.getPageTitle().get(lang);
            String docId = navItem.getId();

            // Concat the page's class hierarchy
            StringBuilder pageClasses = new StringBuilder();
            Class currentCls = page.getClass();

            while(currentCls != null && currentCls != AbstractBlock.class) {
                pageClasses.append(currentCls.getSimpleName());
                pageClasses.append(" ");
                currentCls = currentCls.getSuperclass();
            }

            // Fetch the content by a real request
            String content = getContentOfPage(navItem);

            org.jsoup.nodes.Document jsoupDoc = Jsoup.parse(content);
            String cleanContent = jsoupDoc.text();
            if (title.isEmpty() && cleanContent.isEmpty()) {
                continue;
            }

            // Analyze meta tags
            String description="", author="", keywords="";
            for (Element meta : jsoupDoc.getElementsByTag("meta")) {
                switch (meta.attr("name")) {
                    case "description":
                        description = meta.attr("content");
                        break;

                    case "author":
                        author = meta.attr("content");
                        break;

                    case "keywords":
                        keywords = meta.attr("content");
                        break;
                }
            }

            try {

                HashMap<String, Object> fieldMap = new HashMap<>();
                fieldMap.put(Field.pageId.toString(), id);
                fieldMap.put(Field.title.toString(), title);
                fieldMap.put(Field.url.toString(), navItem.getURL());
                fieldMap.put(Field.content.toString(), cleanContent);
                fieldMap.put(Field.language.toString(), lang);
                fieldMap.put(Field.displayFrom.toString(), page.getDisplayFrom());
                fieldMap.put(Field.displayTo.toString(), page.getDisplayTo());
                fieldMap.put(Field.modified.toString(), page.getModified());
                fieldMap.put(Field.pageClass.toString(), page.getClass().getSimpleName());
                fieldMap.put(Field.classHierarchy.toString(), pageClasses.toString());
                fieldMap.put(Field.author.toString(), author);
                fieldMap.put(Field.description.toString(), description);
                fieldMap.put(Field.keywords.toString(), keywords);

                onUpdateIndex(fieldMap, page, navItem, lang);

                IndexResponse response = client.prepareIndex(config.index(), "page")
                        .setSource(fieldMap)
                        .setId(docId)
                        .execute()
                        .actionGet();

                logger.debug("ElasticSearchProvider: " + (response.isCreated() ? "Added ":"Updated ") + page + " [" + lang + "] in index '" + response.getIndex());

            } catch (Exception e) {
                logger.warn("Elastic: Error while adding/updating index. URL: " + navItem.getURL());
                e.printStackTrace();
            }
        }

    }

    /**
     * Get the html content of the given url. This is normally done via http request.
     *
     * @param navItem nav item (language version) of the page to be indexed
     * @return the body content of the page
     */
    protected String getContentOfPage(NavigationItem navItem) {

        StringBuilder url = new StringBuilder()
                .append(CMS.getConfig().baseUrl())
                .append(navItem.getURL())
                .append(navItem.getURL().contains("?") ? "&" : "?")
                .append("indexer=true");

        HttpRequest request = HttpRequest.get(url.toString());
        if (request.code() != 200) {
            logger.warn("ElasticSearchProvider: URL " + url + " returned a " + request.code() + ", ignoring this page.");
            return "";
        }

        return request.body();
    }

    /**
     * Override this method to add or enhance / modify index fields of a page
     *
     * @param fieldMap the filled list of fields (do not remove existing ones)
     * @param page the page being indexed
     * @param navItem the current nav item (language version) of the page
     * @param lang the current language
     */
    protected void onUpdateIndex(HashMap<String, Object> fieldMap, PageBlock page, NavigationItem navItem, String lang) {}

    /**
     * Remove pages from index
     * @param navId the block id of the to be deleted page
     */
    @Override
    public void removeFromIndex(String navId) {

        DeleteResponse response = client.prepareDelete(config.index(), "page", navId)
                .execute()
                .actionGet();
        logger.debug("Elastic: Delete request sent for doc id: " + navId);
    }
}
