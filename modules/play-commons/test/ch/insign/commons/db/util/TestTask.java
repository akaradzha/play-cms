package ch.insign.commons.db.util;

import ch.insign.commons.util.Task;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "test_tasks")
@DiscriminatorValue("models.TestTask")
public class TestTask extends Task {

    public static ThreadLocal<String> completionOrder = new ThreadLocal<>();

    private int failTimes = 0;

    public int getFailTimes() {
        return failTimes;
    }

    public void setFailTimes(int failTimes) {
        this.failTimes = failTimes;
    }

    @Override
    public boolean execute() {
        if (failTimes > 0) {
            failTimes--;
            return false;
        } else {
            completionOrder.set(completionOrder.get() + getTag());
            return true;
        }
    }

    @Override
    public void failed() {
        throw new UnsupportedOperationException("TestTask::failed isn't supported in test scope.");
    }

}
