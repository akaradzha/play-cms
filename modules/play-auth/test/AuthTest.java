import ch.insign.playauth.PlayAuthApi;
import ch.insign.playauth.authz.AuthorizationHandler;
import ch.insign.playauth.authz.DefaultDomainPermission;
import ch.insign.playauth.authz.DomainPermission;
import ch.insign.playauth.authz.ObjectIdentity;
import ch.insign.playauth.authz.utils.DummyBean;
import ch.insign.playauth.authz.utils.TestDatabase;
import ch.insign.playauth.controllers.actions.RequiresAuthenticationAction;
import ch.insign.playauth.controllers.actions.WithSubjectAction;
import ch.insign.playauth.inject.*;
import ch.insign.playauth.party.ISOGender;
import ch.insign.playauth.party.Party;
import ch.insign.playauth.party.PartyType;
import ch.insign.playauth.party.address.EmailAddress;
import com.google.common.base.Throwables;
import org.apache.shiro.authc.*;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mockito;
import play.Application;
import play.Mode;
import play.api.mvc.RequestHeader;
import play.inject.guice.GuiceApplicationBuilder;
import play.mvc.Action;
import play.mvc.Http;
import play.mvc.Result;
import play.test.Helpers;
import play.test.WithApplication;

import java.util.Collections;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static play.mvc.Http.Status.FORBIDDEN;
import static play.mvc.Http.Status.OK;
import static play.test.Helpers.contentAsString;
import static play.test.Helpers.inMemoryDatabase;

public class AuthTest extends WithApplication {

    private static final DomainPermission<?> TEST_INSTANCE_PERMISSION = new DefaultDomainPermission<>(
            DummyBean.class.getName(), "name", new ObjectIdentity(DummyBean.class.getName(), UUID.randomUUID().toString()));

    private static final DomainPermission<?> TEST_CLASS_PERMISSION = new DefaultDomainPermission<>(
            DummyBean.class.getName(), "name", new ObjectIdentity(DummyBean.class.getName(), ObjectIdentity.ALL.getIdentifier()));

    private static final String PARTY_PWD = "sword";
    private static final String INCORRECT_PARTY_PWD = "wrong";

    @Rule
    public TestDatabase db = new TestDatabase();

    @Override
    @SuppressWarnings("unchecked")
    public Application provideApplication() {
        return new GuiceApplicationBuilder()// Create a guice application with modules
                .in(Helpers.class.getClassLoader())// Take a class loader provided by Play! helper
                .in(Mode.TEST)// Set an environment mode for configuration
                .configure((Map) inMemoryDatabase())// Explicitly convert to Map, not Configuration
                .bindings(
                        new AccessControlListModule(),
                        new AccessControlListVoterModule(),
                        new AccessControlManagerModule(),
                        new AuthorizationHandlerModule(),
                        new AuthorizationHashRetrievalStrategyModule(),
                        new AuthorizerModule(),
                        new DomainObjectRetrievalStrategyModule(),
                        new EventDispatcherModule(),
                        new OidSidRetrievalStrategyModule(),
                        new PartyManagerModule(),
                        new PartyRoleManagerModule(),
                        new PermissionManagerModule(),
                        new PlayAuthApiModule(),
                        new PlayShiroApiModule()
                )
                .bindings(new PlayAuthModule())
                .build();
    }

    @Override
    public void startPlay() {
        super.startPlay();
        Http.Context.current.set(new Http.Context(1L,
                Mockito.mock(RequestHeader.class),
                Mockito.mock(Http.Request.class),
                Collections.emptyMap(),
                Collections.emptyMap(),
                Collections.emptyMap()));
    }

    @Test
    public void testLoginSuccess() {
        db.jpa.withTransaction(() -> {
            Party party = createUnlockedParty();
            try {
                playAuthApi().login(new UsernamePasswordToken(party.getEmail(), PARTY_PWD));
            } catch (AuthenticationException e) {
                fail("Unlocked party can login with valid UsernamePasswordToken");
            }
        });
    }

    @Test
    public void testRequiresAuthenticationSuccess() {
        db.jpa.withTransaction(() -> {
            Party party = createUnlockedParty();
            playAuthApi().login(new UsernamePasswordToken(party.getEmail(), PARTY_PWD));
        });

        Result result = callAction(okActionWith(app.injector().instanceOf(RequiresAuthenticationAction.class)));

        assertEquals(OK, result.status());
        assertEquals("success", contentAsString(result));
    }

    @Test
    public void testRequiresInstancePermissionSuccess() {
        db.jpa.withTransaction(() -> {
            Party party = createUnlockedParty();
            playAuthApi().getAccessControlManager().allowPermission(party, TEST_CLASS_PERMISSION);
            playAuthApi().login(new UsernamePasswordToken(party.getEmail(), PARTY_PWD));

            Result result = callAction(okActionWith(new RequiresPermissionAction<Object>() {
                @Override
                public DomainPermission<?> permission(Http.Context ctx) {
                    return TEST_INSTANCE_PERMISSION;
                }
            }));

            assertEquals(OK, result.status());
            assertEquals("success", contentAsString(result));
        });
    }

    @Test
    public void testRequiresInstancePermissionFailure1() {
        db.jpa.withTransaction(() -> {
            Party party = createUnlockedParty();

            // Deny any permissions if present in database
            playAuthApi().getAccessControlManager().denyPermission(party, TEST_CLASS_PERMISSION);

            playAuthApi().login(new UsernamePasswordToken(party.getEmail(), PARTY_PWD));

            Result result = callAction(okActionWith(new RequiresPermissionAction<Object>() {
                @Override
                public DomainPermission<?> permission(Http.Context ctx) {
                    return TEST_INSTANCE_PERMISSION;
                }
            }));

            // This test denies an access without accessControlManager, so the result should be only 403, not 200
            assertEquals(FORBIDDEN, result.status());
        });
    }

    @Test
    public void testRequiresInstancePermissionFailure2() {
        db.jpa.withTransaction(() -> {
            Party party = createUnlockedParty();
            playAuthApi().getAccessControlManager().allowPermission(party, TEST_CLASS_PERMISSION);
            playAuthApi().getAccessControlManager().denyPermission(party, TEST_INSTANCE_PERMISSION);
            playAuthApi().login(new UsernamePasswordToken(party.getEmail(), PARTY_PWD));

            Result result = callAction(okActionWith(new RequiresPermissionAction<Object>() {
                @Override
                public DomainPermission<?> permission(Http.Context ctx) {
                    return TEST_INSTANCE_PERMISSION;
                }
            }));

            assertEquals(FORBIDDEN, result.status());
        });
    }

    /*
     * Wrap actions into transaction function to test expectation of any exceptions before JPA transaction error
     */

    @Test(expected = LockedAccountException.class)
    public void testLockedAccountLoginFailure() {
        // FIXME the tests bellow fails without an explicit pass of the persistence unit name
        db.jpa.withTransaction("default", em -> {
            Party party = createLockedParty();
            playAuthApi().login(new UsernamePasswordToken(party.getEmail(), PARTY_PWD));

            return null;
        });
    }

    @Test(expected = IncorrectCredentialsException.class)
    public void testIncorrectCredentialsLoginFailure() {
        db.jpa.withTransaction("default", em -> {
            Party party = createUnlockedParty();
            playAuthApi().login(new UsernamePasswordToken(party.getEmail(), INCORRECT_PARTY_PWD));

            return null;
        });
    }

    @Test(expected = UnknownAccountException.class)
    public void testUnknownAccountLoginFailure() {
        db.jpa.withTransaction("default", em -> {
            playAuthApi().login(new UsernamePasswordToken("unknown@insign.ch", INCORRECT_PARTY_PWD));

            return null;
        });
    }

	/* TEST HELPERS */

    /**
     * Composes sample "ok" action with the given action
     */
    private Action<?> okActionWith(Action<?> action) {
        action.delegate = new Action() {
            @Override
            public CompletionStage<Result> call(Http.Context ctx) {
                return CompletableFuture.completedFuture(ok("success"));
            }
        };

        return action;
    }

    private Result callAction(Action<?> action) {
        try {
            return action.call(Http.Context.current()).toCompletableFuture().get();
        } catch (Throwable t) {
            throw Throwables.propagate(t);
        }
    }

    private Party createLockedParty() {
        Party party = playAuthApi().getPartyManager().create("Locked Party", PARTY_PWD, new EmailAddress("locked@insign.ch"), ISOGender.MALE, PartyType.PERSON);
        party.setLocked(true);

        return party;
    }

    private Party createUnlockedParty() {
        Party party = playAuthApi().getPartyManager().create("Unlocked Party", PARTY_PWD, new EmailAddress("unlocked@insign.ch"), ISOGender.MALE, PartyType.PERSON);
        party.setLocked(false);

        return party;
    }

    private PlayAuthApi playAuthApi() {
        return app.injector().instanceOf(PlayAuthApi.class);
    }

    /**
     * Help action class to test permission checks.
     * Use it instead of {@link ch.insign.playauth.controllers.actions.RequiresPermissionAction}
     * because it doesn't wrap the action in jpa transaction, and no EM exceptions appear.
     * @param <T>
     */
    abstract public class RequiresPermissionAction<T> extends WithSubjectAction<T> {

        @Override
        protected PlayAuthApi getPlayAuthApi() {
            return app.injector().instanceOf(PlayAuthApi.class);
        }

        @Override
        protected AuthorizationHandler getAuthorizationHandler() {
            return app.injector().instanceOf(AuthorizationHandler.class);
        }

        @Override
        public CompletionStage<Result> doCall(Http.Context ctx) {
            getPlayAuthApi().requirePermission(permission(ctx));
            return this.delegate.call(ctx);
        }

        abstract public DomainPermission<?> permission(Http.Context ctx);
    }

}
