/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.commons.db;

import play.db.jpa.JPA;

import javax.persistence.Query;

public final class DefaultQueryStreamFactory implements QueryStreamFactory {

    @Override
    public <T> QueryStream<T> create(Query query, Class<T> resultClass) {
        return new DefaultQueryStream<>(query, resultClass);
    }

    @Override
    public <T> QueryStream<T> create(String jpql, Class<T> resultClass) {
        return new DefaultQueryStream<>(JPA.em().createQuery(jpql, resultClass), resultClass);
    }

    @Override
    public <T> QueryStream<T> createNamed(String name, Class<T> resultClass) {
        return new DefaultQueryStream<>(JPA.em().createNamedQuery(name, resultClass), resultClass);
    }
}
